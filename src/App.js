import React from "react";
import minhaFoto from "./minha_foto.jpeg";
import logoGithub from "./github.svg";
import logoTelegram from "./telegram.svg";
import logoFacebook from "./facebook.svg";
import Button from "@material-ui/core/Button";
import "./App.css";
import { Grid, Container, Box } from "@material-ui/core";

function App() {
  const handleLinkClick = event => {
    const { id } = event.target.closest(".column-link").dataset;

    const { url } = links.find(link => link.id === parseInt(id));

    window.open(url, "_blank");
  };

  const links = [
    {
      id: 1,
      label: "GitHub",
      url: "https://www.github.com/isaacbatst",
      logo: logoGithub
    },
    {
      id: 2,
      label: "Telegram",
      url: "https://www.t.me/isaacbatst",
      logo: logoTelegram
    },
    {
      id: 3,
      label: "Facebook",
      url: "https://www.facebook.com/yeesisaac",
      logo: logoFacebook
    }
  ];

  return (
    <div className="App">
      <header className="App-header">
        <Box mt={3}>
          <Container>
            <img
              src={minhaFoto}
              className="minha_foto"
              alt="foto de Isaac"
              width={150}
            />
            <h1 className="title">Isaac Batista</h1>
            <h2>Desenvolvedor Web e Mobile</h2>
            <p>
              Entusiasta JS
            </p>
            <p>Atualmente trabalho desenvolvendo APP com o Framework7 e React, e API's com PHP</p>
            <Box mt={4}>
              <Grid direction="row" container justify="center">
                {links.map(link => (
                  <Grid
                    item
                    sm={2}
                    xs={4}
                    data-id={link.id}
                    className="column-link"
                    key={link.id}
                  >
                    <Button variant="contained" onClick={handleLinkClick}>
                      <img src={link.logo} alt={link.label} />
                    </Button>
                  </Grid>
                ))}
              </Grid>
            </Box>
          </Container>
        </Box>
      </header>
    </div>
  );
}

export default App;
